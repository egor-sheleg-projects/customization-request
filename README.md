# ASP.NET Core Introduction

## Purpose
The purpose of this assignment is to gain a practical experience in:
 - customizing the asp.net core HTTP responce
 - adding additional servicies and middleware
 - setting up a custom error page and proper error handling
 - setting default rout and default page
 - adding CSS styles to the web page
## Description 

**Task1**
- You have to add additional code to the StartUp class and customuze an HTTP response.
- Don't add or change anything in the Unit test project.
- Customize default response, add the required middleware and servicies so that the application produces a conditional response depending on the requested path:
    - According to the request pattern `/hello` => **"Hello response."**
    - Request to the root path `/` => initial response **"Hello World."**
- All unit tests pass and are marked GREEN.
- Commit and push your code to your Git repo.
- Check the task with AutoCode.
       
**Task2**
This part of the task is checked both by AutoCode and by mentor.
First, implement the tasks and check it with Autocode:

- Continue working in the same repository. Create second ASP.NET Core project, use the "Empty project" template, and add it to the solution.
- Create a `wwwroot` folder in your project solution and add an `HTML` page to that folder in the project.
- Name the page `Index.html`
- Add some information to the `Index.html` page, for instance, a short personal portfolio info.
- Style the `Index.html` page with `CSS`-so your page looks better.
- If you don't have any experience with CSS, take a look at https://www.codecademy.com/learn/learn-css. 
- Configure ASP.NET Core to serve the `Index.html` page by default after requesting the root path.
- Add a custom Error.html page that should be shown in case of an application error.
- Check that the Error page is displayed if a user requests a non-existing page.
- Set this project as the start-up project for your VS solution.
- Commit and push your code to your Git repo.
- Check the task with AutoCode. 
