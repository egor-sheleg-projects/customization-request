﻿using System.Threading.Tasks;
using CustomizationHttpRequest;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;
using NUnit.Framework;

namespace CustomizationHttpRequest.Tests
{
    public class Tests
    {
        [Test]
        public async Task Test_Fail_When_RouteIsNotSupported()
        {
            // Arrange
            var hostBuilder = new HostBuilder().ConfigureWebHost(webHost =>
            {
                webHost.UseTestServer();
                webHost.UseStartup<Startup>();
            });

            var host = await hostBuilder.StartAsync();
            var client = host.GetTestClient();
            var response = await client.GetAsync("/");

            // Assert
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
            var response2 = await client.GetAsync("/Hello");
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response2.StatusCode);

        }
    }
}
