﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CustomizationHttpRequest
{
    public class Startup
    {
        /// <summary>
        /// //This method gets called by the runtime.
        /// </summary>
        /// <param name="app">App</param>
        /// <param name="env">Enironment</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Use(async (context, next) =>
            {
                if (context.Request.Path.Value.StartsWith("/hello"))
                {
                    await context.Response.WriteAsync("Hello response.");
                }
                await next();
            });

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World.");
            });
        }
    }
}
